# -*- coding: utf-8 -*-

import re

import pytest
import responses

from mysearch.backends import TxtResult
from mysearch.mysearch import SearchRequest
from mysearch.utils import blacklist_if_needed
from mysearch.utils import promote
from mysearch.utils import replace_if_needed
from mysearch.utils import read_list


@pytest.fixture
def results_to_blacklist():
    res = []
    res += [TxtResult("foo",
                      "https://www.foo.fr",
                      "nice website")]
    res += [TxtResult("amazon",
                      "https://www.amazon.fr",
                      "Achetez neuf ou d'occasion")]
    return res

@pytest.fixture
def blacklist():
    blacklist = [
        "# a comment",
        " ",
        "# squeeze amazon",
        "amazon.fr",
        ]
    return blacklist

@pytest.fixture
def replacelist():
    replacelist = [
        "# comment",
        "amazon.fr lalibrairie.fr",
        ]
    return replacelist

@pytest.fixture
def promotelist():
    promotelist = [
        "# comment",
        "amazon.fr http://www.lalibrairie.fr",
        ]
    return promotelist

class TestBlacklist:

    def test_blacklist(self, results_to_blacklist, blacklist=blacklist()):
        for res in results_to_blacklist:
            res = blacklist_if_needed(res, blacklist=blacklist)

        assert results_to_blacklist[1].blacklisted

    def test_replacelist(self, results_to_blacklist, replacelist=replacelist()):
        keywords = "book foo"
        for res in results_to_blacklist:
            res = replace_if_needed(res, keywords, replacelist=replacelist)

        assert "amazon.fr" not in [it.link_url for it in results_to_blacklist]
        assert "lalibrairie.fr" in [it.link_url for it in results_to_blacklist]

    def test_promotelist(self, results_to_blacklist, promotelist=promotelist()):
        keywords = "book foo"
        for res in results_to_blacklist:
            res = promote(res, keywords, promotelist=promotelist)

        assert "amazon.fr" not in [it.link_url for it in results_to_blacklist]
        assert "lalibrairie.fr" in [it.promote_name for it in results_to_blacklist]

class TestReadRemoteList:

    # Mock requests.
    url_re = re.compile(r'http.*')
    body = "\n".join(replacelist())

    def setup(self):
        responses.add(responses.GET, self.url_re,
                  body=self.body,
                  status=200)

    @responses.activate
    def test_read_remote_list(self):
        """Read a list given as url.
        """
        list_with_url = ["http://foo.txt"]
        res = read_list(list_with_url)
        assert res

    @responses.activate
    def test_read_remote_sublist(self):
        list_with_url = [
            "one http://one",
            "http://rst.txt",
        ]
        res = read_list(list_with_url)
        assert res[0].startswith("one")
        assert res[1] == replacelist()[1]
