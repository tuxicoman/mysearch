# Promote a site next by a result. For websites we consider useful, not harmful, but that have more libre/nicer alternatives.
# Format:
# source <space(s)> target
# Target may have {KEYWORDS} to be replaced by '+'-separated search keywords.

https://framagit.org/ehvince/mysearch-lists/raw/master/promotelist.txt
