# MySearch

Mysearch is a metasearch engine that acts as a proxy for search engines like Google, Youtube, Openstreetmap, etc...

It's designed to anonymate your search requests and have a better display of search results (no ads, no sponsored results, compact format)

An public instance is available at https://search.jesuislibre.net/

# License
Copyright (C) 2013   Tuxicoman

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Requirements

Dependencies are:
- python 2.7
- python-twisted
- python-jinja2
- python-pyasn1-modules
- python-pyasn1

# Use

To launch the software, just do :
$ python mysearch.py
Then access the Search engine in your browser through http://localhost:60061/

You can edit settings in the mysearch.cfg file.

Enjoy !
